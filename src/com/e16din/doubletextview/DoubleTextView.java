package com.e16din.doubletextview;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

public class DoubleTextView extends LinearLayout {

	private TextView tvLeft;
	private TextView tvRight;

	public DoubleTextView(Context context, AttributeSet attrs) {
		super(context, attrs);

		LayoutInflater inflater = LayoutInflater.from(getContext());
		inflater.inflate(R.layout.double_textview, this, true);

		tvLeft = (TextView) findViewById(R.id.tv_left);
		tvRight = (TextView) findViewById(R.id.tv_right);

		setLayoutParams(attrs);
	}

	private void setLayoutParams(AttributeSet attrs) {
		this.setOrientation(LinearLayout.HORIZONTAL);

		updateLeftWeight(0.4f);

		TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.DoubleTextView);
		String textLeft = typedArray.getString(R.styleable.DoubleTextView_textLeft);
		String textRight = typedArray.getString(R.styleable.DoubleTextView_textRigth);

		typedArray.recycle();

		setLeftText(textLeft);
		setRightText(textRight);
	}

	public void updateLeftWeight(float leftWeight) {
		LayoutParams paramsLeft = (LayoutParams) tvLeft.getLayoutParams();
		LayoutParams paramsRight = (LayoutParams) tvRight.getLayoutParams();

		paramsLeft.weight = leftWeight;
		paramsRight.weight = 1 - leftWeight;

		paramsLeft.gravity = Gravity.LEFT;
		paramsRight.gravity = Gravity.LEFT;

		tvLeft.setLayoutParams(paramsLeft);
		tvRight.setLayoutParams(paramsRight);
	}

	public void updateLeftStyle(int styleId) {
		tvLeft.setTextAppearance(getContext(), styleId);
	}

	public void updateRightStyle(int styleId) {
		tvRight.setTextAppearance(getContext(), styleId);
	}

	public void setLeftText(CharSequence text) {
		tvLeft.setText(text);
	}

	public void setRightText(CharSequence text) {
		tvRight.setText(text);
	}

	public TextView getLeftTextView() {
		return tvLeft;
	}

	public TextView getRightTextView() {
		return tvRight;
	}
}
